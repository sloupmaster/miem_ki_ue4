// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "miem_ki_project1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MIEM_KI_PROJECT1_API Amiem_ki_project1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
