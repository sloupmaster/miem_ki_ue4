
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "ConnectionManager.generated.h"

UCLASS()
class MIEM_KI_PROJECT1_API AConnectionManager : public AActor
{
	GENERATED_BODY()

public:
	FHttpModule * Http;

	/* The actual HTTP call */
	UFUNCTION()
		void MyHttpCall();

	/*Assign this function to call when the GET request processes sucessfully*/
	void OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Sets default values for this actor's properties
	AConnectionManager();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//public:
	//	// Called every frame
	//	virtual void Tick(float DeltaTime) override;


};
